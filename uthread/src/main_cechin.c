#include <stdio.h> // Usado para reconhecer o NULL!
#include "./include/uthread.h" // Considera a estrutura de diretórios solicitada

void *func0(void *arg) {
	/* Funcionalidades da thread */
}

void *func1(void *arg) {
	/* Funcionalidades da thread */
}

int main(int argc, char *argv[]) {

	uth_tid id0, id1;

	uth_init();

	id0 = uth_create(func0, NULL);
	id1 = uth_create(func1, NULL);

	uth_wait(id0);
	uth_wait(id1);

	return 0;
}
