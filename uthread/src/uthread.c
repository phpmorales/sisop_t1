/*
*
* Implementação da uthread.c v1.1 
*
*/
#include <stdio.h>
#include <stdlib.h>
#include <ucontext.h>
#include "../include/fifo_scheduler.h"
#include "../include/uthread.h"

/* 
 * Identificador de thread
 */
typedef int uth_tid; 

/*
 * Declaração de variáveis globais relativas as filas 
 */
objthread_t *th_fifo_first;
objthread_t *th_fifo_last;
objthread_t *th_running;
objthread_t th_main;
ucontext_t th_next;

fifo_t *list_able;
fifo_t *list_blocked;
fifo_t *list_zoombie;

/*
 * Variável global para controle do número de threads 
 */
int th_n;

/*
 * Cabeçalho das funções implementadas que não estão no uthread.h
 */
int uth_thinit(objthread_t *th);
uth_tid uth_getid();
int uth_scheduler(int operation);
int uth_close();
int uth_set(const ucontext_t *ucp);
int uth_get(ucontext_t *ucp);
void uth_make(ucontext_t *ucp, void (*func)(), int argc, int argv);
int uth_swap(ucontext_t *oucp, ucontext_t *ucp);
/******************************************************************************
Função: Inicialização da libuthread
Saída:	0 (zero), se realizada com sucesso
        não-zero, se ocorreu algum erro
******************************************************************************/
//  inicializa a FIFO e cria a primeira thread (main)
int uth_init(void){
	list_able = fifo_create();
	list_blocked = fifo_create();
	list_zoombie = fifo_create();
	
	th_n = 0;
    th_fifo_first = EMPTY;
    th_fifo_last = EMPTY;
    th_running = &th_main;

   	return uth_thinit(&th_main);
}

int uth_thinit(objthread_t *th){
    th->tid = th_n++;
	th->list_prev = EMPTY;
    th->list_next = EMPTY;
    th->block_list = EMPTY;
    th->state_expected = EMPTY;
    th->th_context.uc_link = OFF;
    th->th_context.uc_stack.ss_sp = th->th_stack;
    th->th_context.uc_stack.ss_size = TH_STACK;
    th->th_context.uc_stack.ss_flags = OFF;
    return getcontext(&th->th_context);
}

uth_tid uth_getid(){
	return th_running->tid;
}

/******************************************************************************
Função: Cria uma nova thread
Entra:	start_routine: ponteiro para a função que implementa a thread
		arg: ponteiro para os parâmetros que podem ser passados para a thread,
			na sua criação.
Saída:	Retorna o identificador da thread criada, se for um valor positivo;
		Em caso de erro, retorna um valor negativo.
******************************************************************************/
int ctrl_first = 1;

uth_tid uth_create ( void (*start_routine)(void*), void *arg ){
    char th_stack[SIGSTKSZ];
    objthread_t th_new;
	th_new.tid = th_n;
	th_new.flag_join = OFF;

    th_n++; 
    
    getcontext(&(th_new.th_context));
    th_new.th_context.uc_stack.ss_sp = th_stack;
    th_new.th_context.uc_stack.ss_size =  sizeof(th_stack);
	th_new.th_context.uc_link = &th_next;
	makecontext(&(th_new.th_context), (void (*)(void))start_routine, 0, arg);

    //adiciona thread na fila de aptos
    fifo_enqueue(list_able,th_new);

    if(ctrl_first == ON)
    {
        ctrl_first = OFF;
        setcontext(&th_new.th_context);
    }

    uth_scheduler(SCHEDULE_SWAP);
    return th_new.tid;
}

/******************************************************************************
Função: Libera a CPU para uso de outra thread
Saída:	Retorna uma indicação de erro.
		Se o valor retornado for 0 (zero),
			a função foi realizada com sucesso e pode continuar a executar
		se for diferente de zero, houve erro.
******************************************************************************/
int uth_yield( void ){
    printf("  |YELD|  ");
    uth_scheduler(SCHEDULE_SWAP);

    return OFF;
}
/******************************************************************************
Função: Solicitar o seu bloqueio para aguardar o encerramento de uma thread filho
Entra: 	tid: identificador da thread filho (aquela cujo término deve ser aguardado)
Saída:	Retorna uma indicação de erro.
		Se o valor retornado for 0 (zero),
			a thread filha encerrou e pode continuar a executar
		se for diferente de zero, houve erro.
******************************************************************************/
int uth_wait( uth_tid tid ){
	fifonode_t *node_aux;
	objthread_t th;
	
	if(!fifo_empty(list_able)){
		node_aux = fifo_gethead(list_able);
		th = node_aux->th;
		fifo_dequeue(list_able);
		th.flag_join = tid;
		fifo_enqueue(list_blocked, th);
		uth_scheduler(SCHEDULE_NEXT);
		return ON;
	}
	else{
		return OFF;	
	}
		
}

/******************************************************************************
Função: 
Entrada:
Saída:	
******************************************************************************/
int uth_scheduler (int operation){
	printf("\n FIFO SCHEDULER\n");	
	
	if(!fifo_empty(list_able)){
		fifonode_t *node_next;
		fifonode_t *node_tmp;
		node_tmp = fifo_gethead(list_able);

		printf("----------------------------\n");
		fifo_print(list_able);

		if(operation == SCHEDULE_NEXT){	
			fifo_requeue(list_able);
		}
		
		printf("\nAble List:  ");
		fifo_print(list_able);
		printf("----------------------------\n");

		node_next = fifo_gethead(list_able);
		printf("\n| Current %d  -  Next %d |\n", node_tmp->th.tid, node_next->th.tid);

		if(node_tmp != node_next){
			swapcontext(&node_tmp->th.th_context, &node_next->th.th_context);
		}
		else{
			setcontext(&node_tmp->th.th_context);
		}
	}
	return ON;
}

/******************************************************************************
Função: Finalizar threads
		encerra um processo removendo-o da fila de aptos e inserido-o na fila de zombies
		alem disso, verifica se existe algum processo pai aguardando o processo, em caso
		positivo remove o processo pai da fila de bloqueados e insere na fila de aptos novamente
Saída:
******************************************************************************/
int flag_close = ON;
int uth_close (){
	getcontext(&th_next);
	
	if(flag_close != ON){
		fifonode_t *node_first;
		fifonode_t *node_tmp;
		objthread_t th, uth;
		
		node_first = fifo_gethead(list_able);
		th = node_first->th;
	
		printf("\nFinalizando thread %d!", th.tid);
		
		fifo_dequeue(list_able);
		
		fifo_enqueue(list_zoombie, th);
	
		node_tmp = fifo_remove(list_blocked, th.tid);

		if(node_tmp != EMPTY){
			uth = node_tmp->th;
			fifo_enqueue(list_able, uth);					
		}
	
		th_n--;
	
		if(!fifo_empty(list_able))
			uth_scheduler(SCHEDULE_NEXT);
	}
	
	flag_close = OFF;
	
	return ON;
}



int uth_set(const ucontext_t *ucp){
	return setcontext(ucp);

}	

int uth_get(ucontext_t *ucp){
	return getcontext(ucp);
}
	
void uth_make(ucontext_t *ucp, void (*func)(), int argc, int argv){
	makecontext(ucp, func,argc, argv);

}

int uth_swap(ucontext_t *oucp, ucontext_t *ucp){
	return swapcontext(oucp, ucp);
}









