/*
*
* Implementação da fifo_scheduler.c v1.0
*
*/
#include <stdio.h>
#include <stdlib.h>
#include <ucontext.h>
#include "../include/uthread.h"
#include "../include/fifo_scheduler.h"

/******************************************************************************
Função: 
Entrada:
Saída:	
******************************************************************************/
fifo_t* fifo_create (void){
   fifo_t* f = (fifo_t*) malloc(sizeof(f));
   f->node_head = f->node_tail = EMPTY;
   return f;
}

/******************************************************************************
Função: 
Entrada:
Saída:	
******************************************************************************/
int fifo_empty (fifo_t *f){
	if(f->node_head == EMPTY && f->node_tail == EMPTY)
		return ON;
	else
		return OFF;	
}

/******************************************************************************
Função: 
Entrada:
Saída:	
******************************************************************************/
fifonode_t *fifo_getfirst (fifo_t *f){
	if (fifo_empty(f))
		return (fifonode_t*) EMPTY;
	
	fifonode_t *node_tmp = f->node_head;

	return node_tmp;
}

/******************************************************************************
Função: 
Entrada:
Saída:	
******************************************************************************/
fifonode_t *fifo_getlast (fifo_t *f){
	if (fifo_empty(f))
		return (fifonode_t*) EMPTY;
	
	fifonode_t *node_tmp = f->node_tail;

	return node_tmp;
}

/******************************************************************************
Função: 
Entrada:
Saída:	
******************************************************************************/
int fifo_enqueue (fifo_t *f, objthread_t th){
	fifonode_t *node_new = (fifonode_t*) malloc(sizeof(node_new));
	
	if(node_new == EMPTY) 
		return OFF;
	
	node_new->th = th;
	node_new->node_next = EMPTY;
	node_new->node_prev = f->node_tail;
	
	if(fifo_empty(f)) 
		f->node_head = node_new;
	else 
		f->node_tail->node_next = node_new;

	f->node_tail = node_new;

	return ON;
}

/******************************************************************************
Função: 
Entrada:
Saída:	
******************************************************************************/
int fifo_dequeue (fifo_t *f){
	fifonode_t *node_tmp;

	if(fifo_empty(f))
		return OFF;

	if(f->node_head == f->node_tail)
		f->node_tail = EMPTY;

	node_tmp = f->node_head;
	f->node_head = f->node_head->node_next;

	free(node_tmp);

	return ON;
}

/******************************************************************************
Função: 
Entrada:
Saída:	
******************************************************************************/
int fifo_requeue(fifo_t *f){
	if(fifo_empty(f))
		return OFF;
	if(f->node_tail != f->node_head && f->node_head->node_next != EMPTY){
		fifonode_t *node_tmp;

		node_tmp = f->node_head->node_next;
		node_tmp->node_prev = EMPTY;
		f->node_head->node_prev = f->node_tail;
		f->node_tail->node_next = f->node_head;
		f->node_tail = f->node_head;
		f->node_head->node_next = EMPTY;
		f->node_head = node_tmp;
	
		return ON;
	}
	else{
		return OFF;	
	}
}

void fifo_print (fifo_t *f){
	printf("Threads id da fifo:");
	
	fifonode_t *node_tmp;

	if(f->node_head != EMPTY){
		node_tmp = f->node_head;
		while(node_tmp != EMPTY){
			printf(" %d ", node_tmp->th.tid);		
		}
	}
}

/******************************************************************************
Função: 
Entrada:
Saída:	
******************************************************************************/
fifonode_t *fifo_remove(fifo_t *f, int flag_join){
	fifonode_t *node_tmp = f->node_head;
	fifonode_t *node_prev = f->node_begin;
	fifonode_t *node_aux;
	
	int flag_found = OFF;
	
	if(fifo_empty(f))
		return EMPTY;

	if(f->node_begin->th.flag_join == flag_join){
		flag_found = ON;
		node_aux = f->node_begin;
		f->node_begin = EMPTY;
		f->node_tail = EMPTY;
	}
	else{
		while(node_tmp != EMPTY){
			if(node_tmp->th.flag_join === flag_join){
				flag_found = ON;
				node_aux = node_tmp;
				node_prev->node_next = node_tmp->node_next;
				
				if(node_tmp == f->node_next)
					f->node_tail = node_prev;
		
				free(node_tmp);
				node_tmp = node_tmp->node_next;		
			}
		}
	}
	
	if(flag_found == ON){
		return node_aux;
	}
	else{
		return EMPTY;	
	}
}
