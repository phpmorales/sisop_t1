#ifndef uth_tid

typedef int uth_tid; /* Identificador de thread */

/******************************************************************************
Fun��o: Inicializa��o da libuthread
Sa�da:	0 (zero), se realizada com sucesso
        n�o-zero, se ocorreu algum erro
******************************************************************************/
int uth_init(void);

/******************************************************************************
Fun��o: Cria uma nova thread
Entra:	start_routine: ponteiro para a fun��o que implementa a thread
		arg: ponteiro para os par�metros que podem ser passados para a thread,
			na sua cria��o.
Sa�da:	Retorna o identificador da thread criada, se for um valor positivo;
		Em caso de erro, retorna um valor negativo.
******************************************************************************/
uth_tid uth_create(void (*start_routine)(void*), void *arg);

/******************************************************************************
Fun��o: Libera a CPU para uso de outra thread
Sa�da:	Retorna uma indica��o de erro.
		Se o valor retornado for �0� (zero),
			a fun��o foi realizada com sucesso e pode continuar a executar
		se for diferente de zero, houve erro.
******************************************************************************/
int uth_yield(void);

/******************************************************************************
Fun��o: Solicitar o seu bloqueio para aguardar o encerramento de uma thread filho
Entra: 	tid: identificador da thread filho (aquela cujo t�rmino deve ser aguardado)
Sa�da:	Retorna uma indica��o de erro.
		Se o valor retornado for �0� (zero),
			a thread filha encerrou e pode continuar a executar
		se for diferente de zero, houve erro.
******************************************************************************/
int uth_wait(uth_tid tid);


#endif


















