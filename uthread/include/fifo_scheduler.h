#ifndef uth_tid

typedef int uth_tid; /* Identificador de thread */

/*
 * Define número total de estados da FSM 
 */
#define N_STATE 4 	/* Apto - Executando - Bloqueado - Zoombie */

/*
 * Define a posição de cada estado no vetor th_states
 */ 
#define READY 	0 	/* Define estado Apto */
#define RUNNING	1 	/* Define estado Executando */
#define BLOCK	2 	/* Define estado Bloqueado */
#define ZOOMBIE	3 	/* Define estado Zoombie */

/*
 * Define histórico de estados de cada thread 
 */
#define ST_EMPTY		0	/* Thread não existia neste ciclo */
#define ST_CURRENT		1	/* Estado atual da thread */
#define ST_LAST			2	/* Última estado da thread */
#define ST_PENULT		3	/* Penúltimo estado da thread */
#define ST_ANTPENULT	4	/* Antepenúltimo estado da thread */

/*
 * Define variáveis de controle de retornos 
 */
#define ON		1 	 /* Estado desligado */
#define OFF		0 	 /* Estado ligado */
#define ERROR	-1   /* Estado de erro */
#define EMPTY	NULL /* Definição de NULL */

/*
 * Define operações do escalonador
 */
#define SCHEDULE_NEXT 0
#define SCHEDULE_SWAP 1

/*
 *  Cada thread terá uma pilha de 128kb
 */
#define TH_STACK 128*1024

/*
 * Abstração de uma uthread:
 * - tid: código de identificação da thread (tid).
 * - th_context: ponteiro para o contexto de execução da thread.
 * - th_state: vetor de histórico de estados da thread
 */ 

typedef struct objthread{
	uth_tid tid;
	ucontext_t th_context;
	char th_stack[TH_STACK];
	int th_state[N_STATE];
	int *state_expected;
	int flag_join;
	struct objthread *list_prev;
	struct objthread *list_next;
	struct objthread *block_list;	
}objthread_t;

/*
 * Definição do node da fifo
 */
typedef struct fifonode{
	objthread_t th;
	struct fifonode *node_next;
	struct fifonode *node_prev;
}fifonode_t;

/*
 * Definição da fifo
 */
typedef struct fifo{
	fifonode_t *node_head;
	fifonode_t *node_tail;
}fifo_t;


/******************************************************************************
Função: 
Entrada:
Saída:	
******************************************************************************/
fifo_t *fifo_create (void);

/******************************************************************************
Função: 
Entrada:
Saída:	
******************************************************************************/
int fifo_empty(fifo_t *f);

/******************************************************************************
Função: 
Entrada:
Saída:	
******************************************************************************/
int fifo_enqueue (fifo_t *f, objthread_t th);

/******************************************************************************
Função: 
Entrada:
Saída:	
******************************************************************************/
int fifo_dequeue (fifo_t *f);

/******************************************************************************
Função: 
Entrada:
Saída:	
******************************************************************************/
void fifo_print (fifo_t *f);

/******************************************************************************
Função: 
Entrada:
Saída:	
******************************************************************************/
fifonode_t *fifo_gethead (fifo_t *f);

/******************************************************************************
Função: 
Entrada:
Saída:	
******************************************************************************/
fifonode_t *fifo_gettail (fifo_t *f);

/******************************************************************************
Função: 
Entrada:
Saída:	
******************************************************************************/
void fifo_requeue(fifo_t *f);

/******************************************************************************
Função: 
Entrada:
Saída:	
******************************************************************************/
fifonode_t *fifo_remove(fifo_t *f, int flag_join);

#endif
